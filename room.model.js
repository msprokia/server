var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Room',Schema({
    _id: Schema.Types.ObjectId,
    block:{
        type: String,
        require: true,
    },     
    roomno:{
        require: true,
        type: Number
    },
    roomtype:{
        required: true,
        type: String,
    },
    rentalrate:{
        required: true,
        type: Number
    },
    student:{
        type: String
    },
}));