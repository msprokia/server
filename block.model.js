var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Block',Schema({
    _id: Schema.Types.ObjectId,
    college:{
        type: String,
        require: true,
        },
    name:{
        type: String,
        required: true,
        validate:{
            validator: function(text){
                return text.length > 0;
            },
            message: "Empty name is not allowed"
        }
    }
}));