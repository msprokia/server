var express  = require('express');
var app      = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var cors = require('cors');

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(cors());

var College = require('./college.model');
var Block = require('./block.model');
var Room = require('./room.model');

const port = 3000;

mongoose.connect('mongodb://localhost/hostel',{
   useNewUrlParser: true,
   useUnifiedTopology: true
 })
 .then(() => console.log("Successfully connect to MongoDB."))
 .catch(err => console.error("Connection error", err));

app.post('/college', (req, res)=>{
      console.log("Adding new college...");
      var collegeObj = {
         "_id": new mongoose.Types.ObjectId(),
         "name": req.body.name,
         "address": req.body.address
      }
      var newCollege = new College(collegeObj);
      newCollege.save((err, college)=>{
         if(err)
            res.status(400).send("There is an error adding new college.");
         else
            res.status(200).json(college);
      })
});

app.post('/block', (req, res)=>{
      console.log('Adding new block...');
      var blockObj = {
         "_id": new mongoose.Types.ObjectId(),
         "college": req.body.college,
         "name": req.body.name,
      }

      var newBlock = new Block(blockObj);
      newBlock.save((err, block)=>{
         if(err)
            res.status(400).send("There is an error adding new block.");
         else
            res.status(200).json(block);
      })
   });

app.post('/room', (req, res)=>{
      console.log('Adding new room...');
      var roomObj = {
         "_id": new mongoose.Types.ObjectId(),
         "block": req.body.block,
         "roomno": req.body.roomno,
         "roomtype": req.body.roomtype,
         "rentalrate": req.body.rentalrate,
         "student": req.body.student
      }

      var newRoom = new Room(roomObj);
      newRoom.save((err, room)=>{
         if(err)
            res.status(400).send("There is an error adding new room.");
         else
            res.status(200).json(room);
      })
   });

   app.get('/college/:college_id',(req, res)=>{
      College.findById({_id:req.params.college_id}).exec((err, college)=>{
         if(err)
          res.status(400).send(err);
       else
          res.status(200).json(college);
    });
   });

   app.get('/block/:block_id',(req, res)=>{
      Block.findById({_id:req.params.block_id}).exec((err, block)=>{
         if(err)
          res.status(400).send(err);
       else
          res.status(200).json(block);
    });
   });

   app.get('/room/:room_id',(req, res)=>{
      Room.findById({_id:req.params.room_id}).exec((err, room)=>{
         if(err)
          res.status(400).send(err);
       else
          res.status(200).json(room);
    });
   });

   app.get('/college',(req, res)=>{
      console.log('Getting all college...');
      College.find({}).exec((err, colleges)=>{
         if(err)
            res.status(400).send(err);
         else
            res.status(200).json(colleges);
      })
     });

     app.get('/block',(req, res)=>{
      console.log('Getting all block...');
      Block.find({}).exec((err, blocks)=>{
         if(err)
            res.status(400).send(err);
         else
            res.status(200).json(blocks);
      })
     });

     app.get('/room',(req, res)=>{
      console.log('Getting all room...');
      Room.find({}).exec((err, rooms)=>{
         if(err)
            res.status(400).send(err);
         else
            res.status(200).json(rooms);
      })
     })

     app.get('/apply',(req, res)=>{
      console.log('Getting apply list...');
      Room.find({student:""}).exec((err, rooms)=>{
         if(err)
            res.status(400).send(err);
         else
            res.status(200).json(rooms);
      })
     })

     app.put('/college/:college_id',(req, res)=>{
        console.log('Editing a college...');
        College.findByIdAndUpdate({_id:req.params.college_id}, {
         $set: req.body
       }, (err, college) => {
         if (err) {
           return next(err);
         } else {
           res.status(200).json(college);
           console.log('College successfully updated!')
         }
       })
     });

     app.put('/block/:block_id',(req, res)=>{
      console.log('Editing a block...');
      Block.findByIdAndUpdate({_id:req.params.block_id}, {
       $set: req.body
     }, (err, block) => {
       if (err) {
         return next(err);
       } else {
         res.status(200).json(block);
         console.log('Block successfully updated!')
       }
     })
   });
   
   app.put('/room/:room_id',(req, res)=>{
      console.log('Editing a room...');
      Room.findByIdAndUpdate({_id:req.params.room_id}, {
       $set: req.body
     }, (err, room) => {
       if (err) {
         return next(err);
       } else {
         res.status(200).json(room);
         console.log('Room successfully updated!')
       }
     })
   });

   app.put('/apply/:room_id',(req, res)=>{
      console.log('Applying a room...');
      Room.findByIdAndUpdate({_id:req.params.room_id}, {
       $set: req.body
     }, (err, room) => {
       if (err) {
         return next(err);
       } else {
         res.json(room);
         console.log('Room successfully updated!')
       }
     })
   });

   app.delete('/college/:college_id',(req,res)=>{
      console.log("Deleting the college...");
      College.findByIdAndDelete({_id:req.params.college_id}).exec((err, college)=>{
         if(err)
          res.status(400).send(err);
       else
          res.status(200).json(college);
    })
   }); 

   app.delete('/block/:block_id',(req,res)=>{
      console.log("Deleting the block...");
      Block.findByIdAndDelete({_id:req.params.block_id}).exec((err, block)=>{
         if(err)
          res.status(400).send(err);
       else
          res.status(200).json(block);
    })
   }); 

   app.delete('/room/:room_id',(req,res)=>{
      console.log("Deleting the room...");
      Room.findByIdAndDelete({_id:req.params.room_id}).exec((err, room)=>{
         if(err)
          res.status(400).send(err);
       else
          res.status(200).json(room);
    })
   }); 

app.listen(port,()=>{
   console.log("App is listening on port", port);
})

